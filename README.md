# build-container-pipeline

Pipeline for maintenance of my custom build agent.

## Goals
- [ ] Create pipeline to build a fresh Docker container, download latest desired toolings (pinned) and push it to Dockerhub.